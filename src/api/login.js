import request from '@/utils/request'
import qs from 'qs'

export function loginByUsername(username, password) {
  return request({
    url: '/webLogin/auth',
    method: 'post',
    data: qs.stringify({
      username,
      password
    })
  })
}

export function getUserInfo(token) {
  return request({
    url: '/system/baseSysUser/getMyUserInfo',
    method: 'post'
  })
}

