import request from '@/utils/request'
import qs from 'qs'

/**
 * 获取列表
 * @param limit
 * @param offset
 */
export function page(limit, offset, searchName) {
  return request({
    url: '/system/notice/getPage',
    method: 'post',
    data: qs.stringify({
      limit,
      offset,
      searchName
    })
  })
}

/**
 * 获取详情
 * @param id
 */
export function getInfo(id) {
  return request({
    url: '/system/notice/getInfo',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}
