import request from '@/utils/request'
import qs from 'qs'

/**
 * 获取列表
 * @param limit
 * @param offset
 */
export function page(limit, offset, name) {
  return request({
    url: '/system/appUser/getPage',
    method: 'post',
    data: qs.stringify({
      limit,
      offset,
      name
    })
  })
}

/**
 * 获取详情
 * @param id
 */
export function getInfo(id) {
  return request({
    url: '/system/appUser/getInfo',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}

/**
 * 删除
 * @param id
 */
export function delInfo(id) {
  return request({
    url: '/system/appUser/delInfo',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}

/**
 * 新增或编辑
 * @param id
 * @param name
 * @param menuKey
 * @param pId
 * @param type
 */
export function addOrEdit(id, name, mobile, seniorLevel, state) {
  return request({
    url: '/system/appUser/addOrEdit',
    method: 'post',
    data: qs.stringify({
      id, name, mobile, seniorLevel, state
    })
  })
}

/**
 * 获取用户组织列表
 * @param userGroupId
 */
export function getUserGroupList(userId) {
  return request({
    url: '/system/appUser/getUserGroupList',
    method: 'post',
    data: qs.stringify({
      userId
    })
  })
}

/**
 * 获取用户组织详情
 * @param userGroupId
 */
export function getUserGroup(userGroupId) {
  return request({
    url: '/system/appUser/getUserGroup',
    method: 'post',
    data: qs.stringify({
      userGroupId
    })
  })
}

/**
 * 新增或编辑用户组织
 * @param userGroupId
 * @param name
 * @param menuKey
 * @param pId
 * @param type
 */
export function addOrEditUserGroup(userGroupId, userId, businessGroups, store, roadStore, position) {
  return request({
    url: '/system/appUser/addOrEditUserGroup',
    method: 'post',
    data: qs.stringify({
      userGroupId, userId, businessGroups, store, roadStore, position
    })
  })
}

/**
 * 删除用户组织
 * @param userGroupId
 */
export function delUserGroup(userGroupId) {
  return request({
    url: '/system/appUser/delUserGroup',
    method: 'post',
    data: qs.stringify({
      userGroupId
    })
  })
}
