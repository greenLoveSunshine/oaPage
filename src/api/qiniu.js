import request from '@/utils/request'

export function getToken() {
  return request({
    url: '/api/upload/fileToken', // 假地址 自行替换
    method: 'post'
  })
}
