import request from '@/utils/request'
import qs from 'qs'

/**
 * 获取列表
 * @param limit
 * @param offset
 */
export function page(limit, offset, name, roleKey) {
  return request({
    url: '/system/baseSysUser/getPage',
    method: 'post',
    data: qs.stringify({
      limit,
      offset,
      name,
      roleKey
    })
  })
}

/**
 * 获取详情
 * @param id
 */
export function getInfo(id) {
  return request({
    url: '/system/baseSysUser/getInfo',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}

/**
 * 删除
 * @param id
 */
export function delInfo(id) {
  return request({
    url: '/system/baseSysUser/del',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}

/**
 * 新增或编辑
 * @param id
 * @param name
 * @param menuKey
 * @param pId
 * @param type
 */
export function addOrEdit(id, username, roldId, email, mobile, state, vipendtime, num, password) {
  return request({
    url: '/system/baseSysUser/addOrEdit',
    method: 'post',
    data: qs.stringify({
      id, username, roldId, email, mobile, state, vipendtime, num, password
    })
  })
}
