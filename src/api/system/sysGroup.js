import request from '@/utils/request'
import qs from 'qs'

/**
 * 获取列表
 * @param limit
 * @param offset
 */
export function page(limit, offset, name) {
  return request({
    url: '/system/groupList/getPage',
    method: 'post',
    data: qs.stringify({
      limit,
      offset,
      name
    })
  })
}

/**
 * 获取详情
 * @param id
 */
export function getInfo(id) {
  return request({
    url: '/system/groupList/getInfo',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}

/**
 * 删除
 * @param id
 */
export function delInfo(id) {
  return request({
    url: '/system/groupList/delInfo',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}

/**
 * 新增或编辑
 * @param id
 * @param name
 * @param menuKey
 * @param pId
 * @param type
 */
export function addOrEdit(id, name, type, pId) {
  return request({
    url: '/system/groupList/addOrEdit',
    method: 'post',
    data: qs.stringify({
      id, name, type, pId
    })
  })
}

/**
 * 获取组织列表
 * @param id
 */
export function getGroupList(type, pId) {
  return request({
    url: '/system/groupList/getGroupList',
    method: 'post',
    data: qs.stringify({
      type, pId
    })
  })
}
