import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/views/layout/Layout'

Vue.use(Router)

/** note: sub-menu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
    affix: true                  if true, the tag will affix in the tags-view
  }
 **/
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/errorPage/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'appUser',
    children: [
      {
        path: 'appUser',
        component: () => import('@/views/app/appUser'),
        name: 'app用户',
        meta: { title: 'app用户', icon: '', noCache: true, affix: true }
      }
    ]
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

export const asyncRoutes = [
  {
    path: '/email',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/app/email'),
        name: '邮件',
        meta: { title: '邮件', icon: '' }
      }
    ]
  },
  {
    path: '/notice',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/app/notice'),
        name: '公告',
        meta: { title: '公告', icon: '' }
      }
    ]
  },
  {
    path: '/plan',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/app/plan'),
        name: '计划',
        meta: { title: '计划', icon: '' }
      }
    ]
  },
  {
    path: '/journal',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/app/journal'),
        name: '备忘录',
        meta: { title: '备忘录', icon: '' }
      }
    ]
  },
  {
    path: '/contract',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/app/contract'),
        name: '合同',
        meta: { title: '合同', icon: '' }
      }
    ]
  },
  {
    path: '/official',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/app/official'),
        name: '公文',
        meta: { title: '公文', icon: '' }
      }
    ]
  },
  {
    path: '/sysManager',
    component: Layout,
    redirect: 'noRedirect',
    name: '系统管理',
    meta: {
      title: '系统管理',
      icon: ''
    },
    children: [
      {
        path: 'sysUser',
        component: () => import('@/views/system/sysUser'),
        name: '后台用户',
        meta: { title: '后台用户', icon: '', noCache: true }
      },
      {
        path: 'sysGroup',
        component: () => import('@/views/system/sysGroup'),
        name: '组织管理',
        meta: { title: '组织管理', noCache: true }
      }
    ]
  }
]
